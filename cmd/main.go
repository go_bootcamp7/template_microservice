package main

import (
	"fmt"
	"net"

	"gitlab.com/learn-microservice/template_microservice/config"
	u "gitlab.com/learn-microservice/template_microservice/genproto/user"
	"gitlab.com/learn-microservice/template_microservice/pkg/db"
	"gitlab.com/learn-microservice/template_microservice/pkg/logger"
	"gitlab.com/learn-microservice/template_microservice/service"
	grpcclient "gitlab.com/learn-microservice/template_microservice/service/grpc-client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	connDb, err := db.ConnectToDB(cfg)
	if err != nil {
		fmt.Println("Error connect postgres", err.Error())
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Error("Error while grpc connection", logger.Error(err))
	}

	userService := service.NewUserService(connDb, log, grpcClient)

	lis, err := net.Listen("tcp", cfg.UserServicePort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	u.RegisterUserServiceServer(s, userService)

	log.Info("main: server running",
		logger.String("port", cfg.UserServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}

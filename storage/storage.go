package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/learn-microservice/template_microservice/storage/postgres"
	"gitlab.com/learn-microservice/template_microservice/storage/repo"
)

type IStorage interface {
	User() repo.UserStorageI
}

type storagePg struct {
	db       *sqlx.DB
	userRepo repo.UserStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:       db,
		userRepo: postgres.NewUserRepo(db),
	}
}

func (s storagePg) User() repo.UserStorageI {
	return s.userRepo
}

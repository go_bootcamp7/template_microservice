package repo

import (
	u "gitlab.com/learn-microservice/template_microservice/genproto/user"
)

type UserStorageI interface {
	CreateUser(*u.UserRequest) (*u.UserResponse, error)
	GetUserById(*u.UserId) (*u.UserResponse, error)
	GetUsersAll(*u.UserListReq) (*u.Users, error)
	UpdateUser(*u.UserUpdateReq) (*u.UserResponse, error)
	GetUserByPostId(id int64) (*u.UserResponseForPost, error)
}
